package com.example.lesson18.controller;

import com.example.lesson18.model.ResultBoolean;
import com.example.lesson18.model.User;
import com.example.lesson18.model.UserID;
import com.example.lesson18.model.UserTemplate;
import com.example.lesson18.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@Tag(name="UserEntity", description = "API for user")
@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class UserController {

    private final UserService userService;
    private final String endpoint ="/user";

    public UserController(UserService userService) {

        this.userService = userService;
    }
    @Operation(summary = "Get user by id")
    @GetMapping(path = endpoint, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User getUser (@RequestParam int id) throws ResponseStatusException {
        User user = userService.getUser(id);
        if (user ==null)
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "entity not found");
        return userService.getUser(id);
    }

    @Operation(summary = "Create user by name, age and amount")
    @PostMapping(path=endpoint, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public int createUser(@RequestBody UserTemplate temp) {
        return userService.createUser(temp);
    }

    @Operation(summary = "Delete user by id")
    @DeleteMapping(path= endpoint, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultBoolean deleteUser(@RequestBody UserID userid) {
        return userService.deleteUser(userid.getId());
    }

    @Operation(summary = "check application")
    @GetMapping(path = endpoint + "/check/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean checkUser (@PathVariable int id) throws ResponseStatusException  {
        Boolean res = userService.checkUser(id);
        if (res==null)
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "entity not found");
        return userService.checkUser(id);
    }

    @Operation(summary = "update user")
    @PutMapping(path=endpoint + "/update", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateUser(@RequestBody User user) throws ResponseStatusException{
        if (!userService.updateUser(user)){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "entity not found");
        }
    }

    @Operation(summary = "Get all users")
    @GetMapping(path = endpoint + "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<User> getAll (){
        return userService.getAll();
    }

}
