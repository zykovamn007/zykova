package com.example.lesson18.service;


import com.example.lesson18.model.ResultBoolean;
import com.example.lesson18.model.User;
import com.example.lesson18.model.UserTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface UserService {

    User getUser(int id);
    int createUser(UserTemplate user);
    ResultBoolean deleteUser(int id);
    Boolean checkUser(int id) ;
    Boolean updateUser(User user) ;
    List<User> getAll() ;
}
