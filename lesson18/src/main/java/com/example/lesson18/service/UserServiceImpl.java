package com.example.lesson18.service;

import com.example.lesson18.entity.UserEntity;
import com.example.lesson18.model.ResultBoolean;
import com.example.lesson18.model.User;
import com.example.lesson18.model.UserTemplate;
import com.example.lesson18.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private ModelMapper modelMapper = new ModelMapper();
    private UserRepository userRepository;


    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUser(int id) throws ResponseStatusException {
        Optional <UserEntity> userEntityOptional = userRepository.findById(id);
        if (userEntityOptional.isPresent()) {
            return modelMapper.map(userEntityOptional.get(), User.class);
        }else {
            return null;
        }
    }

    @Override
    public int createUser(UserTemplate user) {
        UserEntity userEntity = modelMapper.map(user, UserEntity.class);
        userRepository.saveAndFlush(userEntity);
        return userEntity.getId();
    }

    @Override
    public ResultBoolean deleteUser(int id) {
        ResultBoolean res = new ResultBoolean();
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            res.setSuccess(true);
        }else {
            res.setSuccess(false);
        }
        return res;
    }

    @Override
    public Boolean checkUser(int id)  {
        Optional <UserEntity> userEntityOptional = userRepository.findById(id);
        if (userEntityOptional.isPresent())
            return modelMapper.map(userEntityOptional.get(),User.class).checkUser();
        else
            return null;
    }

    @Override
    public Boolean updateUser(User user)  {
        if (userRepository.existsById(user.getId())) {
            UserEntity userEntity = modelMapper.map(user, UserEntity.class);
            userRepository.saveAndFlush(userEntity);
            return true;
        }else
            return false;
    }

    @Override
    public List<User> getAll()  {
        List <UserEntity> userEntityList = userRepository.findAll();
        List <User> userList = new ArrayList<User>();
        for (UserEntity userEntity:userEntityList) {
            userList.add (modelMapper.map(userEntity, User.class));
        }
        return userList;
    }


}
