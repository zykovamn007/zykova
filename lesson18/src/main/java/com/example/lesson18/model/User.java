package com.example.lesson18.model;

public class User extends Object {
    private int id;
    private String name;
    private int age;
    private Double amount;

    public Double getAmount() {

        return amount;
    }

    public void setAmount(Double amount) {

        this.amount = amount;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }

    public boolean checkUser() {

        return !name.equals("Bob") && age >= 18 && amount <= age * 100;
    }

    public boolean equals(User user) {

        return (user.getId() == this.getId() && user.getName().equals(this.getName())
                && user.getAge() == this.getAge()
                && Math.abs((user.getAmount() - this.getAmount())) < 0.00001);
    }

}
