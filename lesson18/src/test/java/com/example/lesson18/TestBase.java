package com.example.lesson18;

import com.example.lesson18.repository.UserRepository;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

public class TestBase {

    @LocalServerPort
    private Integer port;
    @Autowired
    protected UserRepository userRepository;

    protected RequestSpecification request;


    @BeforeEach
    public void setup(){
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }



}
