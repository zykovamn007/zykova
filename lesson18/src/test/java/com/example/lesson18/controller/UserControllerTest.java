package com.example.lesson18.controller;

import com.example.lesson18.TestBase;
import com.example.lesson18.data.HelperUser;
import com.example.lesson18.model.User;
import com.example.lesson18.model.UserID;
import com.example.lesson18.model.UserTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;


class UserControllerTest extends TestBase {
    private final String endpoint ="/user";
    private User dataUser = null;
    private ObjectMapper mapper = new ObjectMapper();
    private static ModelMapper modelMapper = new ModelMapper();
    @Autowired
    private HelperUser helperUser;


    @AfterEach
    void afterEach()  {
        if (dataUser!=null) {
            helperUser.deleteUser(dataUser.getId());
            dataUser = null;
        }
    }
    @Test
    void getUserPositive()  {
        dataUser = helperUser.createUser();
        User userResponse = request.param("id",dataUser.getId())
                .get(endpoint)
                .then()
                .statusCode(200)
                .extract()
                .body()
                .as(User.class);
        assertTrue(dataUser.equals(userResponse));
    }


    @Test
    void getUserNegativeNotFound()  {
        helperUser.deleteUser(1);
        request.param("id",1).get(endpoint)
                .then().statusCode(404);

    }
    @Test
    void getUserNegativeWrongFormat()  {
        request.param("id","sdf").get(endpoint)
                .then().statusCode(400);

    }

    @Test
    void createUserPositive() throws JsonProcessingException {
        UserTemplate user = helperUser.randomUserTemplate();
        String jsonBody = mapper.writeValueAsString(user);
        int idResult =
                request.contentType("application/json")
                        .body(jsonBody)
                        .post(endpoint)
                        .then()
                        .statusCode(201)
                        .extract().body().as(int.class);
        dataUser = helperUser.getUser(idResult);
        assertNotNull(dataUser);
    }

    @Test
    void createUserNegativeEmpty() throws JsonProcessingException {
        request.contentType("application/json")
                .body("")
                .post(endpoint)
                .then()
                .statusCode(400);
    }


    @Test
    void deleteUser() throws JsonProcessingException {
        dataUser= helperUser.createUser();
        UserID userID = new UserID();
        userID.setId(dataUser.getId());
        String jsonBody = mapper.writeValueAsString(userID);
        request.contentType("application/json")
                .body(jsonBody)
                .delete(endpoint )
                .then()
                .statusCode(200)
                .body("success",equalTo(true));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "Bob, 30, 100.0",
            "Brian, 17, 50.0",
            "Charles, 40, 4000.01"
    })
    void checkClientFalse (String name, int age, Double amount) {
        UserTemplate userTemplate = new UserTemplate();
        userTemplate.setName(name);
        userTemplate.setAge(age);
        userTemplate.setAmount(amount);
        dataUser = helperUser.createUser(userTemplate);
        assertFalse(request.get(endpoint + "/check/" + dataUser.getId())
                .then().statusCode(200)
                .extract()
                .body()
                .as(Boolean.class));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "Mary, 30, 100.0",
            "Brian, 18, 50.0",
            "Charles, 40, 4000.0"
    })
    void checkClientTrue (String name, int age, Double amount)  {
        UserTemplate userTemplate = new UserTemplate();
        userTemplate.setName(name);
        userTemplate.setAge(age);
        userTemplate.setAmount(amount);
        dataUser = helperUser.createUser(userTemplate);
        assertTrue(request.get(endpoint + "/check/" + dataUser.getId())
                .then().statusCode(200)
                .extract()
                .body()
                .as(Boolean.class));
    }

    @Test
    void checkClientNegativeNotFound ()  {
        helperUser.deleteUser(1);
        request.get(endpoint + "/check/" + 1)
                .then().statusCode(404);
    }

    @Test
    void checkClientNegativeWrongFormat ()  {
        request.get(endpoint + "/check/sdfsf")
                .then().statusCode(400);
    }

    @Test
    public void swaggerTesting() throws IOException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("schemaSwagger.json");
        JSONObject schemaSwagger = new JSONObject(new JSONTokener(is));
        is.close();
        request.contentType("application/json")
                .get("v2/api-docs")
                .then()
                .assertThat()
                .body(matchesJsonSchema(String.valueOf(schemaSwagger)));
    }
    @Test
    void getAllValidationJson () throws IOException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("schemaUserAll.json");
        JSONObject schema = new JSONObject(new JSONTokener(is));
        is.close();
        dataUser = helperUser.createUser();
        request.contentType("application/json")
                .get(endpoint + "/all")
                .then().statusCode(200)
                .body(matchesJsonSchema(String.valueOf(schema)));
    }


    void getAllNull()  {
        helperUser.deleteAllUser();
        assertTrue(request.get(endpoint + "/all")
                .then().statusCode(200)
                .extract()
                .body()
                .as(List.class).isEmpty());

    }

    @Test
    void updateUserPositive() throws JsonProcessingException {
        dataUser = helperUser.createUser();
        UserTemplate user = helperUser.randomUserTemplate();
        dataUser.setName(user.getName());
        dataUser.setAge(user.getAge());
        dataUser.setAmount(user.getAmount());
        String jsonBody = mapper.writeValueAsString(dataUser);
        request.contentType("application/json")
                .body(jsonBody)
                .put(endpoint + "/update")
                .then()
                .statusCode(200);
        User updUser = helperUser.getUser(dataUser.getId());
        assertTrue(dataUser.equals(updUser));
    }

    @Test
    void updateUserNegativeNotUser() throws JsonProcessingException {
        UserTemplate userTemplate = helperUser.randomUserTemplate();
        dataUser = modelMapper.map(userTemplate, User.class);
        helperUser.deleteUser(1);
        dataUser.setId(1);
        String jsonBody = mapper.writeValueAsString(dataUser);
        request.contentType("application/json")
                .body(jsonBody)
                .put(endpoint + "/update")
                .then()
                .statusCode(404);
    }

    @Test
    void updateUserNegativeEmpty()  {
        request.contentType("application/json")
                .body("")
                .put(endpoint + "/update")
                .then()
                .statusCode(400);
    }

}