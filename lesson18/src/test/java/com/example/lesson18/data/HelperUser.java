package com.example.lesson18.data;

import com.arakelian.faker.model.Person;
import com.arakelian.faker.service.RandomPerson;
import com.example.lesson18.entity.UserEntity;
import com.example.lesson18.model.User;
import com.example.lesson18.model.UserTemplate;
import com.example.lesson18.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Random;

@Component
public class HelperUser {
    private final UserRepository userRepository;
    private static ModelMapper modelMapper = new ModelMapper();


    public HelperUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public  UserTemplate randomUserTemplate() {
        Random rnd = new Random();
        UserTemplate userTemplate = new UserTemplate();
        Person person = RandomPerson.get().next();;
        userTemplate.setName(person.getFirstName() );
        userTemplate.setAge(person.getAge());
        userTemplate.setAmount((double) rnd.nextInt(10000) + 1);
        return userTemplate;
    }

    public  User createUser()  {
        return createUser(randomUserTemplate());
    }
    public  User createUser(UserTemplate user)  {
        UserEntity userEntity = modelMapper.map(user, UserEntity.class);
        userRepository.saveAndFlush(userEntity);
        return modelMapper.map(userEntity, User.class);
    }

    public  void deleteUser(int id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        }
    }

    public  User getUser(int id) {
        Optional<UserEntity> userEntityOptional = userRepository.findById(id);
        if (userEntityOptional.isPresent())
            return modelMapper.map(userEntityOptional.get(), User.class);
        else
            return null;
    }

    public  void deleteAllUser()  {
        userRepository.deleteAll();
    }


}
